/**
 * Copyright 2019 the original author or authors.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.thierrysquirrel.core.factory;

import com.github.thierrysquirrel.autoconfigure.RocketProperties;
import com.google.common.util.concurrent.ThreadFactoryBuilder;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * ClassName: ThreadPoolFactory
 * Description:
 * date: 2019/4/27 19:52
 *
 * @author ThierrySquirrel
 * @since JDK 1.8
 */
public class ThreadPoolFactory {
	private ThreadPoolFactory() {
	}

	// 自定义线程池工厂
	public static ThreadPoolExecutor createConsumeThreadPoolExecutor(RocketProperties rocketProperties) {
		// 初始化消费者线程数
		Integer threadNums = rocketProperties.getCreateConsumeThreadNums();

		ThreadFactory threadFactory = new ThreadFactoryBuilder()
				.setNameFormat("InitializeConsumerListener").build();
		// 创建线程池
		return new ThreadPoolExecutor(threadNums,
				threadNums,
				0,
				TimeUnit.SECONDS,
				new LinkedBlockingQueue<>(1024),
				threadFactory,
				// TODO
				new ThreadPoolExecutor.AbortPolicy()
		);
	}

	// 生产者线程池
	// 《规约》中提到 线程资源必须通过线程池提供，不允许在应用中自行显式创建线程
	// 《规约》中还提到，线程池的好处是减少在创建和销毁线程上所消耗的时间以及系统资源的开销，解决资源不足的问题。
	// 如果不使用线程池，有可能造成系统创建大量同类线程而导致消耗完内存或者“过度切换”的问题
	public static ThreadPoolExecutor createProducerThreadPoolExecutor(RocketProperties rocketProperties) {

		Integer threadNums = rocketProperties.getCreateProducerThreadNums();

		ThreadFactory threadFactory = new ThreadFactoryBuilder()
				.setNameFormat("InitializeProducer").build();

		return new ThreadPoolExecutor(threadNums,
				threadNums,
				0,
				TimeUnit.SECONDS,
				new LinkedBlockingQueue<>(1024),
				threadFactory,
				new ThreadPoolExecutor.AbortPolicy()
		);
	}

	public static ThreadPoolExecutor createSendMessageThreadPoolExecutor(RocketProperties rocketProperties) {

		Integer threadNums = rocketProperties.getSendMessageThreadNums();

		ThreadFactory threadFactory = new ThreadFactoryBuilder()
				.setNameFormat("SendMessage").build();

		return new ThreadPoolExecutor(threadNums,
				threadNums,
				0,
				TimeUnit.SECONDS,
				new LinkedBlockingQueue<>(),
				threadFactory,
				new ThreadPoolExecutor.AbortPolicy()
		);
	}

	// 回调线程池
	public static ThreadPoolExecutor createCallbackThreadPoolExecutor(RocketProperties rocketProperties) {

		// 默认初始化线程数，都是CPU数量*2 + 1
		Integer threadNums = rocketProperties.getCallbackThreadNums();

		ThreadFactory threadFactory = new ThreadFactoryBuilder()
				.setNameFormat("callback").build();
		return new ThreadPoolExecutor(threadNums,
				threadNums,
				0,
				TimeUnit.SECONDS,
				new LinkedBlockingQueue<>(),
				threadFactory,
				new ThreadPoolExecutor.AbortPolicy()
		);
	}
}
